FROM node:16

WORKDIR /usr/src/app

COPY . ./

ENV PORT 80

RUN npm install -g typescript
RUN npm install

EXPOSE 80

CMD ["npm", "start"]