# Noć istraživača - Prezentacije

## Uvod

Ovaj repozitorijum predstavlja mesto gde ćete, kao učesnici festivala a zaduženi za uređivanje sadržaja za platformu, biti u mogućnosti da pripremate dizajn i izgled prezentacije u obliku jednostraničnog web sajta.

## Kako započeti rad?

Kako biste započeli rad, potrebno je da uradite sledeće korake:

(Obratiti se Milanu Radomiroviću ukoliko je potrebna pomoć oko bilo kog koraka)

- Instalirajte alatku Git na računarima. Svrha ove alatke je mogućnost da stranice koje napravite pošaljete na platformu kako bismo mogli da je sinhronizujemo sa ostatkom platforme automatski.
- Git alatku možete preuzeti sa ovog link-a: https://git-scm.com/downloads. Prilikom ovog procesa, potrebno je izabrati opciju da vam se Git doda u **PATH**. 
- Instalirajte Visual Studio Code za rad na samom kodu sajta - putem ove alatke bićete u mogućnosti da kasnije upload-ujete (odnosno, preciznije, push-ujete) svoj kod na GitHub repozitorijum. 
- Klonirajte repozitorijum (https://github.com/Marwollo/pmf-ni-postavke-prezentacije.git), i otvoriti preuzet projekat koristeći Visual Studio Code u samom root-u projekta.
- U okviru preuzetog projekta, uočiti folder `Pages`. Ovde ćete praviti svoje sajtove, odnosno web prezentacije. Za primer, uzeti postojeći folder `Naslov_Prezentacije` i kopirati ga, pritom promenivši ime na naslov prezentacije koju pravite.
- Napraviti posebnu granu na Git-u u okviru Visual Studio Code okruženja i nazvati je u sledećem formatu: `pmf_ni_naslov_prezentacije` 
- Ostatak je vrlo trivijalan - u folderu koji kopirate je najobičniji sajt koji sadrži `index.html`, `styles.css` gde ćete praviti svoj sajt.

Sajt možete testirati otvaranjem `index.html`, kao što biste to radili u bilo kom drugom scenariju prilikom pravljenja sajtova.

Za upload-ovanje, tj. push-ovanje grane, obratiti se Milanu Radomiroviću.