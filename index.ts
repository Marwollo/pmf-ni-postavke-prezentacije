import express, { Application, Request, Response } from "express";
import { frameRouter } from "./Core";

const app : Application = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.use("/frame", frameRouter);

app.get("/health", (request : Request, response : Response) => {
    response.send("Shardframe je pokrenut.");
});

app.listen(port, () => {
    console.log("Shardframe sluša na portu: " + port);
});