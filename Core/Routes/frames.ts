import express, { Router } from "express";
import fs from "fs";
import path from "path";

const frameRouter = Router();

fs.readdirSync(path.join("Pages")).forEach((page : string) => { 
    frameRouter.use("/" + page, express.static(path.join("Pages", page)));
});

export { frameRouter };